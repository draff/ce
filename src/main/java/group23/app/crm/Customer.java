package group23.app.crm;

import group23.app.shared.UserCreateRequest;

public class Customer {
    String nickname;
    String firstname;
    String lastname;

    public Customer(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
        if (!(this.lastname.isEmpty() || this.firstname.isEmpty())) {
            this.nickname = (lastname.substring(0, 2) + firstname.substring(0, 2)).toLowerCase();
        } else {
            this.nickname = "default";
        }
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public static Customer create(UserCreateRequest userCreateRequest) {
        return new Customer(userCreateRequest.getFirstname(), userCreateRequest.getLastname());
    }
}
