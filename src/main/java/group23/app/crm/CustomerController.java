package group23.app.crm;

import group23.app.shared.UserCreateRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @PostMapping("/create")
    public ResponseEntity<Customer> createCustomer(@RequestBody UserCreateRequest userCreateRequest) {
        return new ResponseEntity<>(Customer.create(userCreateRequest), HttpStatus.CREATED);
    }
}
