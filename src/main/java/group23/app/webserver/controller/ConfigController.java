package group23.app.webserver.controller;

import group23.app.webserver.service.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/config")
public class ConfigController {
    @Autowired
    private WebService webService;

    @GetMapping("/lenker")
    public String configLenker(Model model) {
        String[] lenker = webService.getLenker();
        model.addAttribute("lenker", lenker);
        return "lenker";
    }

    @GetMapping("/material")
    public String configMaterial(@RequestParam String lenkertyp, Model model, HttpSession session) {
        session.setAttribute("lenkertyp", lenkertyp);
        String[] material = webService.getMaterial(lenkertyp);
        model.addAttribute("material", material);
        return "material";
    }

    @GetMapping("/schaltung")
    public String configSchaltung(@RequestParam String material, Model model, HttpSession session) {
        session.setAttribute("material", material);
        String[] schaltung = webService.getSchaltung((String) session.getAttribute("lenkertyp"));
        model.addAttribute("schaltung", schaltung);
        return "schaltung";
    }


    @GetMapping("/griff")
    public String configGriff(@RequestParam String schaltung, Model model, HttpSession session) {
        session.setAttribute("schaltung", schaltung);
        String[] griff = webService.getGriff((String) session.getAttribute("material"));
        model.addAttribute("griff", griff);
        return "griff";
    }

    @GetMapping("/bestellen")
    public String bestellen(@RequestParam String griff, Model model, HttpSession session) {
        session.setAttribute("griff", griff);
        String bestellung = webService.bestellen((String) session.getAttribute("griff"), (String) session.getAttribute("lenkertyp"), (String) session.getAttribute("material"), (String) session.getAttribute("schaltung"));
        model.addAttribute("bestelltext", bestellung);
        return "bestellen";
    }


}
