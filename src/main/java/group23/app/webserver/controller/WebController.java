package group23.app.webserver.controller;

import group23.app.shared.UserCreateRequest;
import group23.app.webserver.model.User;
import group23.app.webserver.service.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;

@Controller
public class WebController {
    @Autowired
    private WebService webService;

    @PostMapping("/user/create")
    public String userSubmit(@ModelAttribute UserCreateRequest ucr, Model model, HttpSession session) {
        User user = webService.createCustomer(ucr);
        session.setAttribute("user", user);
        return "user";
    }

}
