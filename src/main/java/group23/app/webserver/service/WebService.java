package group23.app.webserver.service;

import group23.app.shared.UserCreateRequest;
import group23.app.webserver.model.User;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WebService {
    RestTemplate restTemplate = new RestTemplate();

    public User createCustomer(UserCreateRequest ucr) {
        return restTemplate.postForObject("http://localhost:8080/customer/create", ucr, User.class);
    }

    public String[] getLenker() {
        return restTemplate.getForObject(
                "https://www.maripavi.at/produkt/lenkertyp", String[].class);
    }

    public String[] getMaterial(String lenker) {
        return restTemplate.getForObject("https://www.maripavi.at/produkt/material?lenkertyp=" + lenker, String[].class);
    }

    public String[] getSchaltung(String lenker) {
        return restTemplate.getForObject("https://www.maripavi.at/produkt/schaltung?lenkertyp=" + lenker, String[].class);
    }

    public String[] getGriff(String material) {
        return restTemplate.getForObject("https://www.maripavi.at/produkt/griff?material=" + material, String[].class);
    }

    public String bestellen(String griff, String lenker, String material, String schaltung){
        return restTemplate.postForObject("https://www.maripavi.at/bestellung?griff="+ griff + "&lenkertyp=" + lenker + "&material=" + material + "&schaltung=" + schaltung, null, String.class);
    }
}
